# Requerimientos
- Git
- Node.js
- NPM o Yarn

# Configuración
## Clonar el proyecto: 
  ```
    git clone https://gitlab.com/misaelabanto/googleformbot.git
  ```
## Instalar paquetes:
  ```
    npm install
  ```

o también:
  ```
    yarn
  ```
## Configurar parámetros:
La configuración se realiza en el archivo `formSchema.js`
El archivo tiene la siguiente estructura:

| Parámetro     | Descripción                                         | Tipo         | Default |
| ------------- | --------------------------------------------------- | ------------ | ------- |
| formulario    | Nombre del formulario                               | `string`     | `""`    |
| url           | URL a donde se hace post para responder<sup>1</sup> | `string`     | `""`    |
| nroRespuestas | Número de respuestas a generar                      | `number`     | `1`     |
| questions     | Lista de preguntas del formulario                   | `[Question]` | `[]`    |

La estructura de `Question` es:

| Parámetro    | Descripción                                                                       | Tipo            | Default |
| ------------ | --------------------------------------------------------------------------------- | --------------- | ------- |
| description  | Descripción de la pregunta (no es necesario que sea idéntica a la del formulario) | `string`        | `""`    |
| name         | Nombre del parámetro en el envío post<sup>2</sup>                                 | `string`        | `""`    |
| alternatives | Lista de alternativas de la pregunta                                              | `[Alternative]` | `[]`    |
| multiple     | Es `true` si la pregunta permite más de una opción                                | `boolean`       | `false` |
| quantity | Cantidad máxima de opciones a obtener, sólo si `multiple=true` | `number` | `1` |

La estructura de `Alternative` es:

| Parámetro | Descripción                                                                    | Tipo     | Default |
| --------- | ------------------------------------------------------------------------------ | -------- | ------- |
| d         | Es la descripción de la alternativa. Debe ser idéntica al formulario de Google | `string` | `""`    |
| p         | Es la probabilidad de la alternativa. Entre 0 y 1                              | `number` | `0`     |

<sup>1</sup> La URL se puede encontrar al revisar el Network en las Chrome Dev Tools al subir una respuesta manualmente

<sup>2</sup> El name de cada pregunta se encuentra también en el Network como se muestra en el gif de abajo:

![Network en las Chrome Dev Tools](docs/network.gif "Network en las Chrome Dev Tools")
# Uso
Se tienen tres opciones:
## 1. Build
Ejecutar el comando 
  ```
    npm run build
  ``` 
para generar la data en base a las preguntas y alternativas especificadas en el archivo `formSchema.json` y la guardará en el archivo `data.txt`. Adicionalmente, creará un archivo llamado `stats.json` que contiene estadísiticas de qué alternativa salió por cada respuesta.

## 2. Post
Ejecutar el comando 
  ```
    npm run post
  ``` 
para subir las respuestas al formulario de Google. En consola se mostrará cada respuesta mientras es subida.

## 3. Start
Ejecutar el comando
  ```
    npm run start
  ```
para generar la data y subir las respuestas al formulario de Google. Es la unión de las opciones anteriores

# Limitaciones
El presente proyecto **no** abarca:
- Formularios con más de una página
- Formularios que exigen autenticación con Google
- Multiple choice grid
- Checkbox grid
- File Upload

# Recomendaciones
- Probar con un formulario **de prueba** antes de subir las respuestas al formulario real. Luego, cuando ya se esté seguro de los resultados, cambiar sólo los `names` de las alternativas y la `url` de respuesta del formulario
- Para respuestas más personalizadas, se puede usar [faker](https://github.com/Marak/Faker.js)
- Este bot no se ha probado con más de 1000 respuestas. Si en caso Google llegara a banear la IP, se recomienda usar un proxy. Usar con cuidado.

# Bugs
Si has encontrado un bug o tienes alguna sugerencia, detállala como un nuevo issue [aquí](https://gitlab.com/misaelabanto/googleformbot/issues).
