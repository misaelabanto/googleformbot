
/**
 * 
 * @param { Array<any> } array
 * @returns { string }
 */
function chooseOne(array) {
  return array[Math.floor(Math.random()*array.length)];
}

/**
 * 
 * @param { Array<any> } array 
 * @param { number } originalSize 
 * @returns { Array<string> }
 */
function chooseMultiple(array, originalSize) {
  let num = Math.ceil(Math.random()*originalSize);
  return [... new Set(array.sort(e => Math.random() - 0.5).slice(0, num))];
}

/**
 * 
 * @param { Array<{ d: string, p: number }> } alternatives
 * @returns { Array<string> } 
 */
function getFullAlternatives(alternatives) {
  let result = [];
  for(let alternative of alternatives) {
    result.push(... new Array(Math.round((alternative.p || 0) * 100)).fill(alternative.d || ''));
  }
  return result;
}

module.exports = {
  chooseOne,
  chooseMultiple,
  getFullAlternatives
}