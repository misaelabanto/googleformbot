const request = require('request');
const colors = require('colors');
const fs = require('fs');
const qs = require('querystring');

const { chooseOne, chooseMultiple, getFullAlternatives } = require('./util');
const schema = require('./formSchema.js');
const FORM_URL = schema.url;
const NUM_RESPONSES = schema.nroRespuestas || 1;
const names = schema.questions.map(q => q.name);
const stats = JSON.parse(fs.readFileSync('./stats.json', {encoding: 'utf-8'}));

const OPTIONS = {
  '--build': generateData,
  '--post': readDataAndPostAll
};

function generateData() {
  console.log('📝 Generando data:'.bold.yellow, schema.formulario.bold);
  console.log('👉 Número de respuestas:'.bold.yellow, colors.bold(NUM_RESPONSES));
  for(let i = 1; i <= NUM_RESPONSES; i++) {
    let response = [];
    for(let question of schema.questions) {
      let answer;
      if(!stats[question.description]) stats[question.description] = {};
      let fullAlternatives = getFullAlternatives(question.alternatives);
      if(question.multiple) {
        answer = chooseMultiple(fullAlternatives, question.quantity || 1);
        for(let it of answer) {
          if(!stats[question.description][it]) stats[question.description][it] = 0;
          stats[question.description][it]++;
        }
        answer = answer.join(',');
      } else {
        answer = chooseOne(fullAlternatives);
        if(!stats[question.description][answer]) stats[question.description][answer] = 0;
        stats[question.description][answer]++;
      }
      response.push(answer);
    }
    fs.writeFileSync('./data.txt', response.join('|') + '\n', { flag: 'a+', encoding: 'utf-8' });
  }
  printStats(stats);
  fs.writeFileSync('./stats.json', JSON.stringify(stats), {flag: 'w+', encoding: 'utf-8'})
}

function printStats(stats) {
  console.log('📈 Reporte de generación de respuestas'.bold.yellow, '**'.bold);
  for(let pregunta in stats) {
    console.log(pregunta.magenta + ':');
    let sum = 0;
    for(let alternativa in stats[pregunta]) {
      sum += stats[pregunta][alternativa];
    }
    for(let alternativa in stats[pregunta]) {
      console.log(' ►'.red, alternativa.bold.blue + '', `(${(stats[pregunta][alternativa]*100/sum).toFixed(2)}%)`);
    }
  }
}

function postResponse(response) {
  return new Promise(resolve => {
    request(FORM_URL, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      body: qs.stringify(response)
    }, (err, res, data) => {
      resolve({err, code: res.statusCode, /* data */});
    });
  });
}

async function readDataAndPostAll() {
  console.log('🚀 Subiendo respuestas:'.bold.yellow, schema.formulario.bold);
  let lines = fs.readFileSync('./data.txt', {encoding: 'utf-8'}).split('\n');
  lines.pop();
  for(let line of lines) {
    let values = line.split('|');
    let entry = {}; cont = 0;
    for(let value of values) {
      let item = value.split(',');
      if(item.length === 1) item = item[0];
      if(item.length !== 0) {
        Object.defineProperty(entry, names[cont], {
          value: item,
          enumerable: true
        });
      }
      cont++;
    }
    console.log(await postResponse(entry));
    console.log(JSON.stringify(entry, null, 2));
  }
}

function main() {
  const ARGS = process.argv.slice(2);
  for(let ARG of ARGS) {
    try { OPTIONS[ARG](); } catch (e) { console.log(`No se reconoce la opción: ${ARG}`); }
  }
}

main();