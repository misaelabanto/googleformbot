module.exports = {
  formulario: "Fake Form",
  url: "https://docs.google.com/forms/u/0/d/e/1FAIpQLScHUatYtfa1AVICU1XdMD_wefD8KjMAdgYA1KfEbkNBIOrBOHw/formResponse",
  nroRespuestas: 10,
  questions: [
    {
      description: "Pregunta 1",
      name: "entry.405903451",
      alternatives: [
        {d: "Uno", p: 0.5},
        {d: "Dos", p: 0.3},
        {d: "Tres", p: 0.2},
        {d: "Cuatro", p: 0.1}
      ],
      multiple: false
    },
    {
      description: "Pregunta 2",
      name: "entry.1328976354",
      alternatives: [
        {d: "AAA", p: 0.1},
        {d: "BBB", p: 0.2},
        {d: "CCC", p: 0.3},
        {d: "DDD", p: 0.2},
        {d: "EEE", p: 0.2}
      ],
      multiple: false
    },
    {
      description: "Pregunta 3",
      name: "entry.2020765714",
      alternatives: [
        {d: "Singular", p: 0.25},
        {d: "Múltiple", p: 0.25},
        {d: "Combinado", p: 0.25},
        {d: "Separado", p: 0.25}
      ],
      multiple: true,
      quantity: 2
    }
  ]
}